﻿String.prototype.capitalise = function () {
    var res = this;

    res = res.substr(0, 1).toUpperCase() + res.substr(1);

    return String(res);
}

String.prototype.camelCaseToSpineCase = function () {
    var res = this;

    res = res.replace(/([A-Z])/g, "-$1").toLowerCase();
    res = res.replace(/[ ]/g, "-");
    res = res.replace(/--/g, "-");

    return String(res);
}

String.prototype.spineCaseToCamelCase = function () {
    var res = this;

    res = res.replace(/[  ]/g, " ");
    res = res.replace(/-/g, " ");
    res = res.replace(/\w\S*/g, function (stringValue) { return stringValue.substr(0,1).toUpperCase() + stringValue.substr(1); });
    res = res.replace(/[ ]/g, "");
    res = res.substr(0, 1).toLowerCase() + res.substr(1);

    return String(res);
}

String.prototype.format = function () {
    var res = this;

    for (var key = 0; key < arguments.length; key++) {
        res = res.replace('{' + key + '}', arguments[key]);
    }

    return String(res);
}