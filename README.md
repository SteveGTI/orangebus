Steve Matthews Orange Bus test submission
=========================================

Javascript / Jasmine test tested on;
* Chome 52.0.2743.60 beta-m (64-bit)
* IE11.0.9600.18449 Update 11.0.35 in both Edge and IE9 emulation

All test cases pass

Solution built with;
* Visual Studio Enterprise 2015 V14.0.24720.00 Update 1
* Windows 7 Professional SP1 with .NET Framework 4.6.01055

All test cases pass (including one added to test long strings for the Reverse test)

Any issues / problems / questions; steven.matthews@cheerful.com