﻿using System.Collections;

namespace SkeletonCode.CurrencyConverter
{

	public class Converter
	{
        // For the lack of a database, lets just place the currency values in a private hashtable
        private Hashtable currencyIndex;

        // Constructor to initialize currency exchange values
        public Converter()
        {
            currencyIndex = new Hashtable()
            {
                {"USD", 1.25 },
                {"GBP", 1 }
            };
        }

		public decimal Convert(string inputCurrency, string outputCurrency, decimal amount)
		{
            // Check that the input and output currency ISO codes are recognised, if one is discovered to be unknown, throw an exception
            // NOTE: All currency inputs are forced to upper case to prevent the possibility of missing an otherwise valid currency code
            if(!currencyIndex.ContainsKey(inputCurrency.ToUpper()) || !currencyIndex.ContainsKey(outputCurrency.ToUpper()))
            {
                throw new System.ArgumentException("Unable to find currency code provided in list of currencies.");
            }
            // Based on the currency codes provided, work out the exchange rate between the two currencies and derive a multiplication factor to apply to the original amount
            decimal multiplicationFactor = System.Convert.ToDecimal(currencyIndex[outputCurrency.ToUpper()]) / System.Convert.ToDecimal(currencyIndex[inputCurrency.ToUpper()]);
            return amount * multiplicationFactor;
		}
	}
}
