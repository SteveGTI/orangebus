﻿using System.Collections;

namespace SkeletonCode.ReversingString
{
	public class StringUtilities
	{
		public string Reverse(string input)
		{
            // SM 20161020: One simple fix for handling the failed null test, check for null input.
            if (input != null)
            {
                // SM 20161020: Lets allow native types (i.e. a Last In First Out LIFO Stack) to take the strain
                // Testing on i7 Vaio shows elapsed time of 1.3 seconds average (circa 120ms runtime) compared to between 27 and 29 seconds
                // using the previous for loop method with a 262145 character string.
                var lifoStack = new Stack(input.ToCharArray());
                return string.Join("", lifoStack.ToArray());
            }

            return string.Empty;
		}
	}
}
