﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkeletonCode.CardGame
{
    class Card : ICard
        {
            public string rank { get; set; }
            public string suit { get; set; }
        }
    
}
