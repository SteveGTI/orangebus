﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkeletonCode.CardGame
{
    class PackOfCards : IPackOfCards
    {
        private Card[] cardDeck = new Card[]
            {
                // Initialize deck in "Bicycle" New Deck Order
                new Card() { suit = "Hearts", rank = "Ace" } ,
                new Card() { suit = "Hearts", rank = "2" },
                new Card() { suit = "Hearts", rank = "3" },
                new Card() { suit = "Hearts", rank = "4" },
                new Card() { suit = "Hearts", rank = "5" },
                new Card() { suit = "Hearts", rank = "6" },
                new Card() { suit = "Hearts", rank = "7" },
                new Card() { suit = "Hearts", rank = "8" },
                new Card() { suit = "Hearts", rank = "9" },
                new Card() { suit = "Hearts", rank = "10" },
                new Card() { suit = "Hearts", rank = "Jack" },
                new Card() { suit = "Hearts", rank = "Queen" },
                new Card() { suit = "Hearts", rank = "King" },
                new Card() { suit = "Clubs", rank = "Ace" } ,
                new Card() { suit = "Clubs", rank = "2" },
                new Card() { suit = "Clubs", rank = "3" },
                new Card() { suit = "Clubs", rank = "4" },
                new Card() { suit = "Clubs", rank = "5" },
                new Card() { suit = "Clubs", rank = "6" },
                new Card() { suit = "Clubs", rank = "7" },
                new Card() { suit = "Clubs", rank = "8" },
                new Card() { suit = "Clubs", rank = "9" },
                new Card() { suit = "Clubs", rank = "10" },
                new Card() { suit = "Clubs", rank = "Jack" },
                new Card() { suit = "Clubs", rank = "Queen" },
                new Card() { suit = "Clubs", rank = "King" },
                new Card() { suit = "Diamonds", rank = "King" },
                new Card() { suit = "Diamonds", rank = "Queen" },
                new Card() { suit = "Diamonds", rank = "Jack" },
                new Card() { suit = "Diamonds", rank = "10" },
                new Card() { suit = "Diamonds", rank = "9" },
                new Card() { suit = "Diamonds", rank = "8" },
                new Card() { suit = "Diamonds", rank = "7" },
                new Card() { suit = "Diamonds", rank = "6" },
                new Card() { suit = "Diamonds", rank = "5" },
                new Card() { suit = "Diamonds", rank = "4" },
                new Card() { suit = "Diamonds", rank = "3" },
                new Card() { suit = "Diamonds", rank = "2" },
                new Card() { suit = "Diamonds", rank = "Ace" },
                new Card() { suit = "Spades", rank = "King" },
                new Card() { suit = "Spades", rank = "Queen" },
                new Card() { suit = "Spades", rank = "Jack" },
                new Card() { suit = "Spades", rank = "10" },
                new Card() { suit = "Spades", rank = "9" },
                new Card() { suit = "Spades", rank = "8" },
                new Card() { suit = "Spades", rank = "7" },
                new Card() { suit = "Spades", rank = "6" },
                new Card() { suit = "Spades", rank = "5" },
                new Card() { suit = "Spades", rank = "4" },
                new Card() { suit = "Spades", rank = "3" },
                new Card() { suit = "Spades", rank = "2" },
                new Card() { suit = "Spades", rank = "Ace" }
            };
        public int Count
        {
            get
            {
                return cardDeck.Length;
            }
        }

        public IEnumerator<ICard> GetEnumerator()
        {
            return ((IEnumerable<ICard>)cardDeck).GetEnumerator();
        }

        public void Shuffle()
        {
            Random r = new Random();
            for (int i = cardDeck.Length; i > 0; i--)
            {
                int j = r.Next(i);
                Card k = cardDeck[j];
                cardDeck[j] = cardDeck[i - 1];
                cardDeck[i - 1] = k;
            }
            //20161022 SM: Lets add a shuffle method to make it simulate real-world card shuffling more closely.
            RiffleShuffle();
        }

        private void RiffleShuffle()
        {
            // Calculate middle of the remaining deck and round up (so the left hand never has more cards than the right)
            var intListMidPoint = (int)Math.Ceiling((double)cardDeck.Count() / 2);
            var leftHand = new Queue(cardDeck.Take(intListMidPoint).ToArray());
            var rightHand = new Queue(cardDeck.Skip(intListMidPoint).ToArray());
            var position = 0;
            while (leftHand.Count > 0)
            {
                cardDeck[position] = (Card)leftHand.Dequeue();
                position++;
                if (rightHand.Count > 0)
                {
                    cardDeck[position] = (Card)rightHand.Dequeue();
                    position++;
                }
            }

        }

        public ICard TakeCardFromTopOfPack()
        {
            // 20161022 SM: Re-read guidance, refactored to take the first card in the pack, not the last
            var returnCard = cardDeck[0];
            if (cardDeck.Length > 1) {
                cardDeck = cardDeck.Skip(1).ToArray();
            } else
            {
                cardDeck = null; // You've taken the last card, the deck is no more (prepare for an exception if you try to take another)
            }
            return returnCard;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
