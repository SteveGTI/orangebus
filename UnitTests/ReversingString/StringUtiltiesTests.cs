﻿using NUnit.Framework;

using SkeletonCode.ReversingString;

namespace UnitTests.ReversingString
{
    [TestFixture]
    public class StringUtiltiesTests
    {

		[TestCase("", "")]
		[TestCase("skeleton", "noteleks")]
		[TestCase(null, "")]
		public void ReverseStringShouldReturnTheStringInTheReverseOrder(string input, string expectedResult)
		{
			StringUtilities stringUtilities = new StringUtilities();
			string result = stringUtilities.Reverse(input);

			Assert.AreEqual(expectedResult, result);
		}

        [TestCase("B","B")]
        public void ReverseStringTestLongString(string input, string expectedResult)
        {
            // 20161020 SM: Append a very large string to the begining of the input and end of the output
            // This test allows performance benchmarks to be obtained for very large strings
            var bigInputString = new string('A', 262144) + input;
            var bigExpectedResultString = expectedResult + new string('A', 262144);
            StringUtilities stringUtilities = new StringUtilities();
            string result = stringUtilities.Reverse(bigInputString);

            Assert.AreEqual(bigExpectedResultString, result);
        }
    }
}
